# npm-install

Test npm install with GitLab NPM registry feature (locally with GDK)

# Setup instrcutions

1. Get auth token for `.npmrc`

        curl -d "@auth.txt" -X POST http://localhost:3001/oauth/token

1. Replace `TOKEN` with auth token in `.npmrc`
1. Make sure you have `@foo/bar` package locally on GDK. See https://gitlab.com/gitlab-org/examples/npm-publish
1. Install `@foo/bar` dependency from GitLab

        npm --verbose install

